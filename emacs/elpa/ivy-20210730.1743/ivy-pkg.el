(define-package "ivy" "20210730.1743" "Incremental Vertical completYon"
  '((emacs "24.5"))
  :commit "c1e53c21bc5563ca08551595b7a6d5bacf5794e4" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("matching")
  :url "https://github.com/abo-abo/swiper")
;; Local Variables:
;; no-byte-compile: t
;; End:
