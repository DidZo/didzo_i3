(define-package "magit-section" "20210813.904" "Sections for read-only buffers"
  '((emacs "25.1")
    (dash "2.18.1"))
  :commit "421be65a325f2988d12868b57386683d3efafbe2" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
