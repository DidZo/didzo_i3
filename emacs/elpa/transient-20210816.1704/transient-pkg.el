(define-package "transient" "20210816.1704" "Transient commands"
  '((emacs "25.1"))
  :commit "e17e2b2f6d7cdf2e27729fd28e3d7600b76ad24a" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("bindings")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:
