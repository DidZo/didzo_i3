(define-package "forge" "20210817.1934" "Access Git forges from Magit."
  '((emacs "25.1")
    (closql "1.0.6")
    (dash "2.18.1")
    (emacsql-sqlite "3.0.0")
    (ghub "3.5.2")
    (let-alist "1.0.6")
    (magit "3.0.0")
    (markdown-mode "2.4")
    (transient "0.3.3")
    (yaml "0.3.3"))
  :commit "9fbd00e97df4bedf00c72911777c207c5cd40b6d" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("git" "tools" "vc")
  :url "https://github.com/magit/forge")
;; Local Variables:
;; no-byte-compile: t
;; End:
